package comp

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/orm"
	conf "gitlab.com/flex_comp/remote_conf"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_db/model/db_strategy"
)

func init() {
	_ = comp.RegComp(new(Strategy))
}

type Strategy struct{}

func (u *Strategy) Init(map[string]interface{}, ...interface{}) error {
	dsn := util.ToString(conf.Get("orm", "strategy"))
	mds := orm.NewModels()
	mds.Set(dsn, &db_strategy.Strategy{})

	return orm.Reg(mds)
}

func (u *Strategy) Start(...interface{}) error {
	return nil
}

func (u *Strategy) UnInit() {
}

func (u *Strategy) Name() string {
	return "comp-strategy"
}
