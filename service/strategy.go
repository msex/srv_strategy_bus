package service

import (
	"context"
	"github.com/bytedance/gopkg/cloud/metainfo"
	"gitlab.com/flex_comp/kitex"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/orm"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_db/model/db_strategy"
	"gitlab.com/msex/model_pb/kitex_gen/external/e_strategy"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_code"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_strategy"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"gitlab.com/msex/model_pb/kitex_gen/srv/s_strategy/strategy"
	"strings"
	"sync"
)

type StrategyImpl struct {
}

func (u *StrategyImpl) Strategy(ctx context.Context, req *e_strategy.ReqStrategy) (res *e_strategy.RspStrategy, err error) {
	res = new(e_strategy.RspStrategy)
	uid, ok := metainfo.GetPersistentValue(ctx, define.MetaUID)
	if !ok {
		res.Code = m_code.Code_ApiKeyInvalid
		return
	}

	queries := []string{
		"uid = ?",
	}

	args := []interface{}{
		uid,
	}

	if req.Ex != m_common.Exchange_EX_None {
		queries = append(queries, "exchange = ?")
		args = append(args, req.Ex)
	}

	if req.ETy != m_common.ExchangeType_ET_None {
		queries = append(queries, "exchange_type = ?")
		args = append(args, req.ETy)
	}

	if req.Symbol != nil {
		queries = append(queries, "base_symbol = ?", "quote_symbol = ?")
		args = append(args, strings.ToLower(req.Symbol.Base), strings.ToLower(req.Symbol.Quote))
	}

	if req.Status != m_strategy.StStatus_ST_None {
		queries = append(queries, "status = ?")
		args = append(args, req.Status)
	}

	var arr []*db_strategy.Strategy
	tx := orm.DB().Where(strings.Join(queries, " AND "), args...).Find(&arr)
	if tx.Error != nil {
		log.Error(tx)
		res.Code = m_code.Code_InternalError
		return
	}

	res.Code = m_code.Code_Ok
	for _, s := range arr {
		res.Strategy = append(res.Strategy, &m_strategy.Strategy{
			Id:  util.ToString(s.ID),
			STy: s.SType,
			Ex:  m_common.Exchange(s.Exchange),
			ETy: m_common.ExchangeType(s.ExchangeType),
			Symbol: &m_symbol.Symbol{
				Base:  s.BaseSymbol,
				Quote: s.QuoteSymbol,
			},
			Status:        m_strategy.StStatus(s.Status),
			Risk:          m_strategy.StRisk(s.Risk),
			FirstCostPer:  s.FirstCostPer,
			ExecLimit:     s.ExecLimit,
			Args:          s.Args,
			ExecCount:     s.ExecCount,
			AvgPrice:      s.AvgPrice,
			Quantity:      s.Quantity,
			OpenCount:     s.OpenCount,
			OrderId:       strings.Split(s.Oids, ","),
			PeriodOrderId: strings.Split(s.PeriodOids, ","),
			Profit:        s.Profit,
			PeriodProfit:  s.PeriodProfit,
			CreateAt:      s.CreateAt,
		})
	}

	return
}

func (u *StrategyImpl) SetStrategy(ctx context.Context, req *e_strategy.ReqSetStrategy) (res *e_strategy.RspSetStrategy, err error) {
	// TODO: 检查余额及其他条件
	// 根据 策略类型分类
	m := make(map[string]*e_strategy.ReqSetStrategy)
	for _, s := range req.Close {
		sTy := s.STy
		v, ok := m[sTy]
		if !ok {
			v = new(e_strategy.ReqSetStrategy)
			m[sTy] = v
		}

		v.Close = append(v.Close, s)
	}

	for _, s := range req.Update {
		sTy := s.STy
		v, ok := m[sTy]
		if !ok {
			v = new(e_strategy.ReqSetStrategy)
			m[sTy] = v
		}

		v.Update = append(v.Update, s)
	}

	for _, s := range req.Open {
		sTy := s.STy
		v, ok := m[sTy]
		if !ok {
			v = new(e_strategy.ReqSetStrategy)
			m[sTy] = v
		}

		v.Open = append(v.Open, s)
	}

	res = &e_strategy.RspSetStrategy{
		Code: m_code.Code_Ok,
	}

	var (
		waitGroup sync.WaitGroup
		mtx       sync.Mutex
	)

	for sTy, s := range m {
		sTy := sTy
		s := s
		waitGroup.Add(1)
		go func() {
			defer waitGroup.Done()

			cli, err := strategy.NewClient(define.CliStrategy(sTy), kitex.CliOpts()...)
			if err != nil {
				log.Error(err)
				return
			}

			r, err := cli.SetStrategy(ctx, s)
			if err != nil || r.Code != m_code.Code_Ok {
				log.ErrorF("%v - %s", r, err)
				return
			}

			mtx.Lock()
			defer mtx.Unlock()

			res.Close = append(res.Close, r.Close...)
			res.Open = append(res.Open, r.Open...)
			res.Update = append(res.Update, r.Update...)
		}()
	}

	waitGroup.Wait()

	res.Code = m_code.Code_Ok
	return
}
